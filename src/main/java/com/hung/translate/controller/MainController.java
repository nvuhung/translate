package com.hung.translate.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hung.translate.model.ResultEnum;
import com.hung.translate.model.ResultModel;
import com.hung.translate.service.ITranslateService;

@Controller
public class MainController {
	@Autowired
	ITranslateService translateService;

	public File convertMultipartFileToFile(MultipartFile multipart) throws IllegalStateException, IOException {
		File file = new File(multipart.getOriginalFilename());
		multipart.transferTo(file);
		return file;
	}
	
	@RequestMapping(value = "/excelToFile", method = RequestMethod.POST)
	@ResponseBody
	public ResultModel translateFromExcelFile(
			@RequestParam(value = "srcFile", required = true) MultipartFile srcFile,
			@RequestParam(value = "destFile", required = false) MultipartFile destFile,
			@RequestParam(value = "prefix", required = false) String prefix,
			@RequestParam(value = "line", required = false) int line) throws Exception {
		if(srcFile == null) {
			return new ResultModel(ResultEnum.RESULT_ERROR, "Source file must not be null!");
		}
		ResultModel translated = translateService.translateFromExcelToList(convertMultipartFileToFile(srcFile), prefix, true);
		if(destFile != null && translated.getResultSuccess() != null) {
			@SuppressWarnings("unchecked")
			List<String> text = (List<String>) translated.getResultSuccess();
			return translateService.addTextToList(text, convertMultipartFileToFile(destFile), line);
		} else {
			return translated;
		}
	}
}
