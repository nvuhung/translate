package com.hung.translate.model;

public enum ResultEnum {
	RESULT_SUCCESS("SUCCESS"), RESULT_ERROR("ERROR");

	private final String text;

	private ResultEnum(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
}
