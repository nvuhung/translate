package com.hung.translate.model;

import java.util.HashMap;

public class ResultModel extends HashMap<Object, Object> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9180094442792573639L;

	public ResultModel() {
		super();
	}
	
	public ResultModel(ResultEnum type, Object obj) {
		if(type == ResultEnum.RESULT_SUCCESS) {
			this.setResultSucess(obj);
		} else if(type == ResultEnum.RESULT_ERROR) {
			this.setResultError(obj);
		}
	}
	
	public Object getResultSuccess() {
		return this.get(ResultEnum.RESULT_SUCCESS.toString());
	}
	
	public void setResultSucess(Object data) {
		this.put(ResultEnum.RESULT_SUCCESS.toString(), data);
	}
	
	public Object getResultError() {
		return this.get(ResultEnum.RESULT_ERROR.toString());
	}
	
	public void setResultError(Object error) {
		this.put(ResultEnum.RESULT_ERROR.toString(), error);
	}
}
