package com.hung.translate.service;

import java.io.File;
import java.util.List;

import com.hung.translate.model.ResultModel;


public interface ITranslateService {
	public ResultModel translateFromExcelToList(File srcFile, String prefix, boolean isUseCommas);
	public ResultModel addTextToList(List<String> texts, File destFile, int line) throws Exception;
}