package com.hung.translate.service;

public final class ConstantManager {
	public static final String EXCEL_EXTENSION = "xls|xlsx";
	public static final String JSON_EXTENSION = "json";
	public static final String PROPERTIES_EXTENSION = "properties";
}
