package com.hung.translate.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.google.common.math.DoubleMath;
import com.hung.translate.model.ResultEnum;
import com.hung.translate.model.ResultModel;
import com.hung.translate.service.CommonUtils;
import com.hung.translate.service.ITranslateService;

@Service
public class TranslateServiceImpl implements ITranslateService {

	@Override
	public ResultModel translateFromExcelToList(File srcFile, String prefix, boolean isUseCommas) {
		List<String> translatedText = new ArrayList<String>();
		try {
			InputStream is = new FileInputStream(srcFile);
			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(is);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> rows = firstSheet.iterator();
			while (rows.hasNext()) {
				Row row = rows.next();
				String key = CommonUtils.buildKeyWithPrefix(getCellValue(row.getCell(0)), prefix);
				String value = getCellValue(row.getCell(1));
				String fullText = "";
				fullText = CommonUtils.buildFullTranslateText(key, value, prefix, isUseCommas);
				translatedText.add(fullText);
			}
			is.close();
			return new ResultModel(ResultEnum.RESULT_SUCCESS, translatedText);
		} catch (IOException e) {
			System.out.println("Error occurred! Cause: " + e.getMessage());
			return new ResultModel(ResultEnum.RESULT_ERROR, translatedText);
		}
	}
	
	@Override
	public ResultModel addTextToList(List<String> texts, File destFile, int line) throws Exception {
		List<String> existedText = FileUtils.readLines(destFile, StandardCharsets.UTF_8);
		if(line > existedText.size() || line < 1) {
			return new ResultModel(ResultEnum.RESULT_ERROR, "Number of line is invalid!");
		} else {
			existedText.addAll(line, texts);
			return new ResultModel(ResultEnum.RESULT_SUCCESS, existedText);
		}
	}
	
	public static String getCellValue(Cell cell) {
		if(cell == null) return "";
		String value = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
		case Cell.CELL_TYPE_BLANK:
			value = cell.getStringCellValue();
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			value = cell.getBooleanCellValue() ? "True" : "False";
			break;
		case Cell.CELL_TYPE_NUMERIC:
			if(DoubleMath.isMathematicalInteger(cell.getNumericCellValue())) {
				value = String.valueOf((int)cell.getNumericCellValue());
			} else {
				value = Double.toString(cell.getNumericCellValue());
			}
		default:
			break;
		}
		return value;
	}
}
