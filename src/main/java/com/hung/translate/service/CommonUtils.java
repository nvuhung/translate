package com.hung.translate.service;

import java.io.File;

import org.apache.commons.lang.StringUtils;

public class CommonUtils {
	
	
	
	public static boolean isFileExist(String fileName) {
		return isFileExist(new File(fileName));
	}
	
	public static boolean isFileExist(File file) {
		return file.exists()  && !file.isDirectory();
	}
	
	public static String getExtensionFile(String fileName) {
		int lastIndex = fileName.lastIndexOf(".");
		if(lastIndex > 0) {
			return fileName.substring(lastIndex);
		} else {
			return "";
		}
	}
	
	public static String buildKeyWithPrefix(String key, String prefix) {
		if(StringUtils.isNotBlank(prefix)) {
			if(!StringUtils.endsWith(prefix, ".")) {
				return prefix + "." + key; 
			} else {
				return prefix + key;
			}
		} else {
			return key;
		}
	}
	
	public static String buildFullTranslateText(String key, String value, String prefix, boolean isUseCommas) {
		String keyWithPrefix = buildKeyWithPrefix(key, prefix);
		if(isUseCommas) {
			return "\"" + keyWithPrefix + "\"" + ":"  + "\"" + value + "\"" + ",";
		} else {
			return "\"" + keyWithPrefix + "\"" + ":"  + "\"" + value + "\"";
		}
	}
	
	public static boolean isValidFileType(String fileName, String fileType) {
		if(StringUtils.isBlank(fileName) || StringUtils.isBlank(fileType)) {
			return false;
		}
		return getExtensionFile(fileName).equals(fileType);
	}
}
