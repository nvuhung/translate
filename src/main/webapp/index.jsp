<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css"/>
<link rel="stylesheet" href="css/style.css"/>
</head>
<body ng-app="app" ng-controller="mainCtrl">
	<div class="container">
		<div class="clearfix">
			<div class="pull-left"><h1>Translate</h1></div>
		</div>
		
		<div class="bd-example">	
			<form ng-submit="uploadFile()" method="POST" enctype="multipart/form-data">
			<fieldset class="form-group col-md-12">
				<div class="col-md-2">
					<label>Source File</label>
				</div>
				<div class="col-md-2">
					<span class="btn btn-primary btn-file">
		                   <i class="fa fa-plus"></i>
		                   <span>Add file</span>
		                   <input type="file" file-model="srcFile" required accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
		            </span>
	            </div>
	            <div class="col-md-8">
	            	<span ng-bind="srcFile.name"></span>
	            </div>
            </fieldset>
            <fieldset class="form-group col-md-12">
            	<div class="col-md-2">
            		Export to File
            	</div>
				<div class="col-md-2">
					<input type="checkbox" ng-model="isUseDestFile" ng-true-value="'Y'" ng-false-value="'N'">
				</div>
			</fieldset>
            <fieldset class="form-group col-md-12" ng-show="isUseDestFile == 'Y'">
				<div class="col-md-2">
					<label>Dest File</label>
				</div>
				<div class="col-md-2">
					<span class="btn btn-primary btn-file">
		                   <i class="fa fa-plus"></i>
		                   <span>Add file</span>
		                   <input type="file" file-model="destFile" accept=".json">
		            </span>
	            </div>
	            <div class="col-md-2">
	            	<span ng-bind="destFile.name"></span>
	            </div>
	            <div class="col-md-4">
	            	<div class="col-md-2">
	            		<label>Line</label>
	            	</div>
	            	<div class="col-md-8">
	            		<input class="form-control" type="text" placeholder="Line" ng-model="line" style="width: 90px;">
	            	</div>
	            </div>
            </fieldset>
            <fieldset class="form-group col-md-12">
            	<div class="col-md-2">
					<label>Prefix</label>
				</div>
				<div class="col-md-10">
            		<input class="form-control" type="text" placeholder="Prefix" ng-model="prefix">
            	</div>
            </fieldset>
            <fieldset class="form-group col-md-12">
			    <div class="col-md-2">
					<label>Result</label>
				</div>
				 <div class="col-md-6">
				 	<!-- http://stackoverflow.com/a/28223151 -->
			    	<textarea id="result1" class="form-control" rows="10" ng-model="result" ng-list="&#10;" ng-trim="false"></textarea>
			    </div>
			    <div class="col-md-2" ng-if="result != undefined && result.length > 0">
<!-- 			    	<button type="button" class="btn" ngclipboard data-clipboard-text="result"> -->
<!-- 					    Copy to clipboard -->
<!-- 					</button> -->

<!-- <button type="button" class="btn btn-warning" clip-copy="getTextFromTextArea()">Copy to clipboard</button> -->
			    </div>
			 </fieldset>
            <fieldset class="form-group col-md-12">
            	<button class="btn btn-danger" type="submit">OK</button>
            </fieldset>
            </form>
		</div>
	</div>

	<script src="lib/jquery-1.11.3.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.js"></script>
	<script src="lib/angular.min.js"></script>
<!-- 	<script src="lib/ngclipboard.min.js"></script> -->
	
<!-- 	<script src="http://asafdav.github.io/ng-csv/javascripts/ngClip.js"></script> -->
<!-- 	<script src="http://cdnjs.cloudflare.com/ajax/libs/zeroclipboard/1.1.7/ZeroClipboard.min.js"></script> -->

	<script src="js/app.js"></script>
</body>
</html>