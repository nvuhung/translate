'use strict';

var app = angular.module('app', []);

//http://uncorkedstudios.com/blog/multipartformdata-file-upload-with-angularjs	
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.controller('mainCtrl', function($scope, $rootScope, $http) {
	
	  $scope.uploadFile = function(){
		  var formData = new FormData();
		  formData.append("srcFile", $scope.srcFile);
		  formData.append("prefix", $scope.prefix);
		  if($scope.isUseDestFile == 'Y') {
			  formData.append("destFile", $scope.destFile);
			  formData.append("line", parseInt($scope.line));
		  } else {
			  formData.append("destFile", null);
			  formData.append("line", 0);
		  }
		  $http({
			  method: 'POST',
			  url: 'excelToFile',
			  headers: { 'Content-Type': undefined},
			  data:  formData,
		  })
	      .success(function(data, status) {                       
	          //alert("Success ... " + status);
	    	 if(data["SUCCESS"] != null) {
	    		$scope.result = data["SUCCESS"];  
	    	 } else {
	    		 alert("Error ... " + data["ERROR"]);
	    	 }
	      })
	      .error(function(data, status) {
             alert("Error ... " + status);
	      });
	  };
	  
	  $scope.getTextFromTextArea = function() {
		  	return $('result1').val();
	  };
});
