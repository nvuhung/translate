<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.css">
</head>
<body>
	<h1>Hello, world!</h1>

	<script src="lib/jquery-1.11.3.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.js"></script>
</body>
</html>