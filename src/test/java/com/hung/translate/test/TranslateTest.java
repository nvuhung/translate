package com.hung.translate.test;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.hung.translate.model.ResultModel;
import com.hung.translate.service.ITranslateService;
import com.hung.translate.service.impl.TranslateServiceImpl;

public class TranslateTest {
	
	ITranslateService translateService;
	
	@Before
	public void before() {
		translateService = new TranslateServiceImpl();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testTranslateFromExcel() throws Exception {
		String srcFile = "E:\\WORKS\\WorkSpace\\myproject\\translate-core\\support\\key.xlsx";
		String destFile = "E:\\test.txt";
		ResultModel texts = translateService.translateFromExcelToList(new File(srcFile), "abc", true);
		if(texts.get("SUCCESS") != null) {
			List<String> translatedText =  (List<String>) texts.get("SUCCESS");
			ResultModel textFromFile = translateService.addTextToList(translatedText, new File(destFile), 2);
			List<String> result =  (List<String>) textFromFile.get("SUCCESS");
			for (String string : result) {
				System.out.println(string);
			}
		}
	}
}
